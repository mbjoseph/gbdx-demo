# Makefile for the noaa demo
VIIRS_DATA = /mnt/work/input/VIIRS/NEW/GITCO-VIVIO_npp_d20150628_t2035460_e2041264_b19004_c20160426144157736220_noaa_ops.h5
SMAP_DATA = /mnt/work/input/SMAP/SMAP_L4_SM_gph_20150628T013000_Vb1010_001.h5
PROCESSING_SCRIPTS = R/smap_read.R R/smap_process.R R/viirs_read.R R/viirs_process.R
PLOTS = /mnt/work/output/plots/scatter1.pdf /mnt/work/output/plots/scatter2.pdf /mnt/work/output/plots/levelplots.pdf /mnt/work/output/plots/surfs.pdf
GEOTIFFS = /mnt/work/output/geotiffs/viirs_ndvi.tif /mnt/work/output/geotiffs/smap.tif
OUTPUT = $(GEOTIFFS) $(PLOTS)

# running either "make" or "make all" will invoke the following line
all: $(OUTPUT)

# convert the hdf5 files to geotiffs with matched projections and save
$(GEOTIFFS): $(VIIRS_DATA) $(SMAP_DATA) $(PROCESSING_SCRIPTS) data/hydrology
	mkdir -p /mnt/work/output/geotiffs
	R CMD BATCH --vanilla R/viirs_read.R
	R CMD BATCH --vanilla R/viirs_process.R
	R CMD BATCH --vanilla R/smap_read.R
	R CMD BATCH --vanilla R/smap_process.R
	rm /mnt/work/output/geotiffs/*_raw.tif

# make some plots and do some simple analysis
$(PLOTS): $(GEOTIFFS) R/make_plots.R data/ecoregions
	mkdir -p /mnt/work/output/plots
	R CMD BATCH --vanilla R/make_plots.R
	R CMD BATCH --vanilla R/write_success.R
	rm Rplots.pdf
	rm *.Rout
