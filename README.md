# NOAA data partnership/GBDX demo

The goal here is to relate soil moisture data (SMAP data from NSIDC & NASA) to NDVI (from VIIRS).
The R implementation is contained in the `R` directory.
The Makefile is the master script for running the analysis in R, which loads the data from `/mnt/work/input/`, processes it, and saves some output in `/mnt/work/output/plots/`.

This project is designed to be run inside of a container with a custom R/Ubuntu environment called `earthlab-r`, which is available on [Docker Hub](https://hub.docker.com/r/mbjoseph/earthlab-r/).
